/*
 * look_and_say.c: calculates Look-and-say sequence.
 *
 * Date: 2017/08/14
 * Author: Yu Yoneda
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_usage(void){
  printf("Usage: prog_name first_member num_calc\n"\
	 "\tfirst_member: give the first member of look and say sequence. Typically this member is \"1\".\n"\
	 "\tnum_calc: give number until which this program calculate look and say sequence.\n\n");
}

int look_and_say(char **member_p){
  int member_size, i, num_same;
  char *result = NULL;
  char tmp_str[3];
  char prev_char;

  member_size = strlen(*member_p);
  result = malloc((2 * member_size + 1) * sizeof(char));
  sprintf(result, "");

  num_same = 1;

  for(i = 1; i < member_size; ++i){
    if((*member_p)[i] == (*member_p)[i-1])
      ++num_same;
    else{
      sprintf(tmp_str, "%1d%c", num_same, (*member_p)[i-1]);
      strcat(result, tmp_str);
      num_same = 1;
    }
  }
  sprintf(tmp_str, "%1d%c", num_same, (*member_p)[member_size - 1]);
  strcat(result, tmp_str);

  free(*member_p);
  *member_p = result;

  return 0;
}  
  
int main(int argc, char *argv[]){
  int num_calc, i;
  char *member = NULL;
  
  if(argc != 3){
    print_usage();
    exit(EXIT_FAILURE);
  }

  member = malloc((strlen(argv[1]) + 1) * sizeof(char));
  sprintf(member, argv[1]);	/* first member */
  num_calc = atoi(argv[2]);	/* number until which this program calculate look and say sequence */
  printf("index\tLook-and-say\n");
  printf("%d\t%s\n", 1, member);
  
  for(i = 0; i < num_calc - 1; ++i){
    look_and_say(&member);
    printf("%d\t%s\n", i + 2, member);
  }

  free(member);
  exit(EXIT_SUCCESS);
}
